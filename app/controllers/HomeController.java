package controllers;

import actors.KafkaLogConsumer;
import actors.LogMessage;
import actors.LogRouter;
import akka.NotUsed;
import akka.actor.*;
import akka.japi.Pair;
import akka.stream.Materializer;
import akka.stream.OverflowStrategy;
import akka.stream.javadsl.*;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.name.Named;
import es.kibu.geoapis.objectmodel.MetricsDefinition;
import es.kibu.geoapis.services.data.DataSources;
import es.kibu.geoapis.services.objectmodel.results.Application;
import es.kibu.geoapis.services.objectmodel.results.ResultValue;
import es.kibu.geoapis.services.objectmodel.results.ResultValueReader;
import org.apache.commons.io.IOUtils;
import org.reactivestreams.Publisher;
import org.slf4j.LoggerFactory;
import play.Configuration;
import play.Logger;
import play.api.Play;
import play.http.HttpEntity;
import play.libs.F;
import play.libs.Json;
import play.libs.ws.WS;
import play.libs.ws.WSClient;
import play.libs.ws.WSRequest;
import play.mvc.*;
import scala.Option;
import scala.reflect.ClassTag;
import services.UrlUtils;
import views.html.appdetails;
import views.html.apps;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;

import static services.UrlUtils.*;
//import play.api.libs.ws.WSClient;
//import views.html.index;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class HomeController extends Controller {

    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */

    //private org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger("controllers.HomeController");

    /*private ActorRef stocksActor;
    private ActorRef userParentActor;
    */private Materializer materializer;
    private ActorSystem actorSystem;

    @Inject
    WSClient ws;

    @Inject
    Configuration config;

    KafkaLogConsumer consumer;
    ActorRef logRouterActor;

    @Inject
    public HomeController(ActorSystem actorSystem,
                          Materializer materializer,
                          @Named("log-router-actor") ActorRef logRouterActor
                          /*@Named("userParentActor") ActorRef userParentActor*/) {
        this.logRouterActor = logRouterActor;
        this.materializer = materializer;
        this.actorSystem = actorSystem;

        //createConsumer();
    }

    public CompletionStage<Result> getData(String appid, String datasource) {

        String cleanDs = datasource.contains("::") ? datasource.substring(datasource.indexOf("::") + 2): datasource;
        String url = getUrl(getDataServicesUrls().getData(appid, cleanDs));

        String rawQueryString = request()._underlyingRequest().rawQueryString();
        WSRequest wsRequest = ws.url(url).setQueryString(rawQueryString);

        return wsRequest.get().thenApply(rs -> rs.getBody()).thenApply(content -> {
            JsonNode data = Json.parse(content);
            if (!(data.has("error") && data.get("error").asBoolean())) {
                JsonNode node = data.get("result");
                return ok(node);
            } else {
                return badRequest(data.get("errorMessage").asText());
            }
        });
    }


    public String getHelpUrl () {
        return config.getString(DOCS_URL_KEY);
    }

    private String quote(String toquote) {
        return "\"" + toquote + "\"";
    }

    private String getQuotedList(List<String> list) {
        String result = "";
        String sep = "";

        for (String s : list) {
            result += sep + quote(s);
            sep = ", ";
        }

        return result;
    }

   /* public CompletionStage<Result> dataFilterParamsEditor1(String appid) {

        String url = getUrl(getDataServicesUrls().getDatasources(appid));

        return ws.url(url).get().thenApply(rs -> rs.getBody()).thenApply(content -> {
            JsonNode datasources = Json.parse(content);
            Option<InputStream> inputStreamOption = Play.current().resourceAsStream("/public/resources/data-filter-schema-parametrized.json");

            if (!(datasources.has("error") && datasources.get("error").asBoolean()) && inputStreamOption.isDefined()) {
                JsonNode node = datasources.get("result");
                DataSources dataSources = Json.fromJson(node, DataSources.class);

                try {
                    List<String> varTitles = getVariablesTitles(dataSources);
                    List<String> varDatasourcesIds = getVariablesDatasources(dataSources);
                    List<String> metricsTitles = getMetricsTitles(dataSources);
                    List<String> metricsDatasources = getMetricsDatasources(dataSources);

                    String filter = IOUtils.toString(inputStreamOption.get(), "UTF-8");
                    String filterSchema = String.format(filter, getQuotedList(varDatasourcesIds), getQuotedList(varTitles),
                            getQuotedList(metricsDatasources), getQuotedList(metricsTitles));

                    return ok(Json.parse(filterSchema));

                } catch (IOException e) {
                    return badRequest(e.getMessage());
                }
            }
            else
            return badRequest(datasources.get("errorMessage").asText());

        });
    }*/

    public CompletionStage<Result> dataFilterParamsEditor(String appid) {

        String url = getUrl(getDataServicesUrls().getDatasources(appid));

        return ws.url(url).get().thenApply(rs -> rs.getBody()).thenApply(content -> {
            JsonNode datasources = Json.parse(content);
            Option<InputStream> inputStreamOption = Play.current().resourceAsStream("/public/resources/data-filter-schema-parametrized-simple.json");

            if (!(datasources.has("error") && datasources.get("error").asBoolean()) && inputStreamOption.isDefined()) {
                JsonNode node = datasources.get("result");
                DataSources dataSources = Json.fromJson(node, DataSources.class);

                try {
                    List<String> varTitles = getDataSourcesListAll(dataSources, true);
                    List<String> varDatasourcesIds = getDataSourcesListAll(dataSources, false);

                    String filter = IOUtils.toString(inputStreamOption.get(), "UTF-8");
                    String filterSchema = String.format(filter, getQuotedList(varDatasourcesIds), getQuotedList(varTitles));

                    return ok(Json.parse(filterSchema));

                } catch (IOException e) {
                    return badRequest(e.getMessage());
                }
            } else
                return badRequest(datasources.get("errorMessage").asText());

        });
    }


    private List<String> getMetricsTitles(DataSources dataSources) {
        return getDataSourcesTitles(dataSources, true);
    }

    private List<String> getMetricsDatasources(DataSources dataSources) {
        return getDataSourcesIds(dataSources, true);
    }

    private List<String> getVariablesTitles(DataSources dataSources) {
        return getDataSourcesTitles(dataSources, false);
    }

    private List<String> getVariablesDatasources(DataSources dataSources) {
        return getDataSourcesIds(dataSources, false);
    }

    private List<String> getDataSourcesTitles(DataSources dataSources, boolean metrics) {
        return getDataSourcesList(dataSources, metrics, true);
    }

    private List<String> getDataSourcesIds(DataSources dataSources, boolean metrics) {
        return getDataSourcesList(dataSources, metrics, false);
    }

    private List<String> getDataSourcesList(DataSources dataSources, boolean metrics, boolean title) {
        List<String> list = new ArrayList<>();
        if (metrics) {
            for (DataSources.MetricsDatasourceInfo metricsDatasourceInfo : dataSources.getMetricsDatasources()) {
                list.add((title) ? metricsDatasourceInfo.getMetricsName() : metricsDatasourceInfo.getDatasourceName());
            }
        } else {
            for (DataSources.VariablesDatasourceInfo variableDatasourceInfo : dataSources.getVariablesDatasources()) {
                list.add((title) ? variableDatasourceInfo.getVariableName() : variableDatasourceInfo.getDatasourceName());
            }
        }
        return list;
    }

    private List<String> getDataSourcesListAll(DataSources dataSources, boolean title) {
        List<String> list = new ArrayList<>();
        for (DataSources.MetricsDatasourceInfo metricsDatasourceInfo : dataSources.getMetricsDatasources()) {
            list.add("metric::" + ((title) ? metricsDatasourceInfo.getMetricsName() : metricsDatasourceInfo.getDatasourceName()));
        }
        for (DataSources.VariablesDatasourceInfo variableDatasourceInfo : dataSources.getVariablesDatasources()) {
            list.add("variable::" + ((title) ? variableDatasourceInfo.getVariableName() : variableDatasourceInfo.getDatasourceName()));
        }
        return list;
    }

    private void getConsumer() {
        if (consumer == null) {
            createConsumer(Sink.actorRef(logRouterActor, new Status.Success("success")));
        }
    }

    public Result json() {
        return ok(MetricsDefinition.class.getResourceAsStream("/metrics-schema.json"));
    }

    public CompletionStage<Result> appEditor() {
        String url = getUrl(getAppServicesUrls().getEditor());
        Logger.debug("APP Edito: " + url);
        return ws.url(url).get().thenApply(rs -> rs.getBody()).thenApply(content -> ok(Json.parse(content)));
    }

    public CompletionStage<Result> getApp(String appId) {
        UrlUtils.ApplicationServicesUrls appServicesUrls = getAppServicesUrls();
        return getById(appId, appServicesUrls);
    }

    private CompletionStage<Result> getById(String appId, UrlUtils.BaseServicesUrls appServicesUrls) {
        String url = getUrl(appServicesUrls.getBaseSelect() + appId);
        Logger.debug("APP Retrieve: " + url);
        return ws.url(url).get().thenApply(rs -> rs.getBody()).thenApply(content -> ok(Json.parse(content)));
    }

    public CompletionStage<Result> execGet(String appid) {
        UrlUtils.BaseServicesUrls appServicesUrls = getExecutionParamsServicesUrls();
        return getById(appid, appServicesUrls);
    }

    public CompletionStage<Result> execCreate(String appid) {
        UrlUtils.BaseServicesUrls appServicesUrls = getExecutionParamsServicesUrls();
        return create(appServicesUrls, appid);
    }

    public CompletionStage<Result> execUpdate(String appid) {
        UrlUtils.BaseServicesUrls appServicesUrls = getExecutionParamsServicesUrls();
        return update(appid, appServicesUrls);
    }

    private CompletionStage<Result> update(String appid, UrlUtils.BaseServicesUrls servicesUrls) {
        String url = getUrl(servicesUrls.getBaseUpdate() + appid);
        Logger.debug("Update: " + url);
        JsonNode jsonNode = request().body().asJson();
        Logger.debug("Update Body: " + jsonNode.toString());
        return ws.url(url).setContentType("application/json").post(jsonNode).thenApply(content -> new Result(content.getStatus(), HttpEntity.fromString(content.getBody(), "UTF-8")));
    }

    public CompletionStage<Result> deleteApp(String appId) {

        String url = getUrl(getAppServicesUrls().getBaseDelete() + appId);
        Logger.debug("APP Delete: " + url);
        return ws.url(url).post("").thenApply(rs -> rs.getBody()).thenApply(content -> ok(Json.parse(content)));
    }

    public CompletionStage<Result> saveApp(String appid) {
        return update(appid, getAppServicesUrls());
    }

    public CompletionStage<Result> createApp() {
        UrlUtils.BaseServicesUrls appServicesUrls = getAppServicesUrls();
        return create(appServicesUrls, null);
    }

    private CompletionStage<Result> create(UrlUtils.BaseServicesUrls appServicesUrls, String appId) {
        String url = getUrl(appServicesUrls.getBaseCreate() + (appId != null ? appId : ""));
        Logger.debug("APP Update: " + url);
        JsonNode jsonNode = request().body().asJson();
        return ws.url(url).setContentType("application/json").post(jsonNode).thenApply(content -> new Result(content.getStatus(), HttpEntity.fromString(content.getBody(), "UTF-8")));
    }

    public Result appDetails(String appid) {
        try {
            ArrayList<String> entries = new ArrayList<>();
            entries.add("Details");
            entries.add("Parameters");
            entries.add("Metrics");
            entries.add("Run");
            entries.add("Data");

            ResultValue<Application> appFromServices = getAppFromServices(appid);
            return ok(appdetails.render(appFromServices.getResult(), entries, request(), getHelpUrl()));
        } catch (IOException | ExecutionException | InterruptedException e) {
            Logger.debug("Error while accessing application details", e);
            e.printStackTrace();
            return badRequest(e.getMessage());
        }
    }

    public Result rawApps() {

        UrlUtils.ApplicationServicesUrls applicationServicesUrls = getAppServicesUrls();
        String baseList = applicationServicesUrls.getBaseList();

        String url = getUrl(baseList);
        JsonNode jsValue = null;
        try {
            jsValue = ws.url(url).get().thenApply(response -> response.asJson()).toCompletableFuture().get();
            return ok(jsValue);

        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return badRequest(e.getMessage());
        }
    }


    public Result apps() {

        UrlUtils.ApplicationServicesUrls applicationServicesUrls = getAppServicesUrls();
        String baseList = applicationServicesUrls.getBaseList();

        String url = getUrl(baseList);
        try {
            JsonNode jsValue = ws.url(url).get().thenApply(response -> response.asJson()).toCompletableFuture().get();
            String json = jsValue.toString();
            ResultValue<List<Application>> listResultValue = ResultValueReader.asApplicationsResult(json);
            if (!listResultValue.isError()) {
                List<Application> applications = listResultValue.getResult();
                return ok(apps.render(applications));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return ok(apps.render(new ArrayList<Application>()));
    }

    public ResultValue<Application> getAppFromServices(String appId) throws IOException, ExecutionException, InterruptedException {
        String baseList = getAppServicesUrls().getBaseSelect() + appId;
        String url = getUrl(baseList);
        JsonNode jsValue = ws.url(url).get().thenApply(response -> response.asJson()).toCompletableFuture().get();
        String json = jsValue.toString();
        ResultValue<Application> applicationResult = ResultValueReader.asApplicationResult(json);
        return applicationResult;
    }

    private String getUrl(String baseList) {
        return config.getString(BASE_SERVICES_URL_KEY) + baseList;
    }

    public CompletionStage<Result> execParamsEditor() {
        String url = getUrl(getExecutionParamsServicesUrls().getEditor());
        Logger.debug("EXEC Editor url: " + url);
        return ws.url(url).get().thenApply(rs -> rs.getBody()).thenApply(content -> ok(Json.parse(content)));
    }

    public CompletionStage<Result> metricsGet(String appid) {
        UrlUtils.BaseServicesUrls appServicesUrls = getMetricsServicesUrls();
        return getById(appid, appServicesUrls);
    }

    public CompletionStage<Result> metricsCreate(String appid) {
        UrlUtils.BaseServicesUrls appServicesUrls = getMetricsServicesUrls();
        return create(appServicesUrls, appid);
    }

    public CompletionStage<Result> metricsUpdate(String appid) {
        UrlUtils.BaseServicesUrls appServicesUrls = getMetricsServicesUrls();
        return update(appid, appServicesUrls);
    }


    public CompletionStage<Result> runEditor(String appid) {
        String url = getUrl(getRunServicesUrls().getEditor()) + appid;
        Logger.debug("EXEC Editor url: " + url);
        return ws.url(url).get().thenApply(rs -> rs.getBody()).thenApply(content -> ok(Json.parse(content)));
    }


    private RunServicesUrls getRunServicesUrls() {
        return UrlUtils.getRunServicesUrls();
    }

    public CompletionStage<Result> run(String appid) {
        Logger.info("Frontend is running task for app: {}", appid);
        JsonNode jsonNode = request().body().asJson();
        Logger.info("configuration: {}", jsonNode);
        String url = getUrl(getRunServicesUrls().getRun(appid));
        return ws.url(url).post(jsonNode).thenApply(rs -> rs.getBody()).thenApply(content -> {
            Logger.info("Service response: {}", content);
            return ok(Json.parse(content));
        });
    }

    public CompletionStage<Result> invalidateCache(String appid) {
        Logger.info("Invalidating cache for app: {}", appid);
        String url = (appid.isEmpty()) ? getUrl(getMetricsServicesUrls().getInvalidate()) : getUrl(getMetricsServicesUrls().getInvalidate(appid));
        return ws.url(url).post("").thenApply(rs -> rs.getBody()).thenApply(content -> {
            Logger.info("Service response: {}", content);
            return ok(Json.parse(content));
        });
    }

    public WebSocket ws() {
        getConsumer();
        return WebSocket.Text.acceptOrResult(request -> {
            if (sameOriginCheck(request)) {
                final CompletionStage<Flow<String, String, NotUsed>> future = wsFutureFlow(request);
                final CompletionStage<F.Either<Result, Flow<String, String, ?>>> stage = future.thenApplyAsync(F.Either::Right);
                return stage.exceptionally(this::logException);
            } else {
                return forbiddenResult();
            }
        });
    }

    private void createConsumer(Sink<LogMessage, NotUsed> sink) {
        consumer = new KafkaLogConsumer();
        consumer.start(actorSystem, config.underlying().getConfig("akka.kafka.consumer"), sink);
    }

    public CompletionStage<Flow<String, String, NotUsed>> wsFutureFlow(Http.RequestHeader request) {
        // create an actor ref source and associated publisher for sink
        final Pair<ActorRef, Publisher<String>> pair = createWebSocketConnections();
        ActorRef webSocketOut = pair.first();
        //createConsumer(Sink.actorRef(webSocketOut, new Status.Success("success")));
        //register the route
        logRouterActor.tell(new LogRouter.RegisterMsg(webSocketOut, request().queryString().get("id")[0]), webSocketOut);

        Publisher<String> webSocketIn = pair.second();

        String id = String.valueOf(request._underlyingHeader().id());
        // Create a user actor off the request id and attach it to the source
        final CompletionStage<ActorRef> userActorFuture = createUserActor(id, webSocketOut);

        // Once we have an actor available, create a flow...
        final CompletionStage<Flow<String, String, NotUsed>> stage = userActorFuture
                .thenApplyAsync(userActor -> createWebSocketFlow(webSocketIn, userActor));

        return stage;
    }

    static class DummyActor extends UntypedActor {
        org.slf4j.Logger logger = LoggerFactory.getLogger(DummyActor.class);

        @Override
        public void onReceive(Object message) throws Throwable {
            logger.info(message.toString());
        }

        public static Props props() {
            ClassTag<DummyActor> tag = scala.reflect.ClassTag$.MODULE$.apply(DummyActor.class);
            return Props.apply(tag);
        }

    }


    public CompletionStage<ActorRef> createUserActor(String id, ActorRef webSocketOut) {
        // Use guice assisted injection to instantiate and configure the child actor.
        /*long timeoutMillis = 100L;
        return FutureConverters.toJava(
                ask(userParentActor, new UserParentActor.Create(id, webSocketOut), timeoutMillis)
        ).thenApply(stageObj -> (ActorRef) stageObj);
        */
        ActorRef ref = actorSystem.actorOf(DummyActor.props());
        return CompletableFuture.completedFuture(ref);
    }


    public Pair<ActorRef, Publisher<String>> createWebSocketConnections() {
        // Creates a source to be materialized as an actor reference.

        // Creating a source can be done through various means, but here we want
        // the source exposed as an actor so we can send it messages from other
        // actors.
        final Source<String, ActorRef> source = Source.actorRef(10, OverflowStrategy.dropTail());

        // Creates a sink to be materialized as a publisher.  Fanout is false as we only want
        // a single subscriber here.
        final Sink<String, Publisher<String>> sink = Sink.asPublisher(AsPublisher.WITHOUT_FANOUT);

        // Connect the source and sink into a flow, telling it to keep the materialized values,
        // and then kicks the flow into existence.
        final Pair<ActorRef, Publisher<String>> pair = source.toMat(sink, Keep.both()).run(materializer);
        return pair;
    }


    public Flow<String, String, NotUsed> createWebSocketFlow(Publisher<String> webSocketIn, ActorRef userActor) {
        // http://doc.akka.io/docs/akka/current/scala/stream/stream-flows-and-basics.html#stream-materialization
        // http://doc.akka.io/docs/akka/current/scala/stream/stream-integrations.html#integrating-with-actors

        // source is what comes in: browser ws events -> play -> publisher -> userActor
        // sink is what comes out:  userActor -> websocketOut -> play -> browser ws events
        final Sink<String, NotUsed> sink = Sink.actorRef(userActor, new Status.Success("success"));
        final Source<String, NotUsed> source = Source.fromPublisher(webSocketIn);
        final Flow<String, String, NotUsed> flow = Flow.fromSinkAndSource(sink, source);

        // Unhook the user actor when the websocket flow terminates
        // http://doc.akka.io/docs/akka/current/scala/stream/stages-overview.html#watchTermination
        return flow.watchTermination((ignore, termination) -> {
            termination.whenComplete((done, throwable) -> {
                Logger.info("Terminating actor {}", userActor);
                //stocksActor.tell(new Stock.Unwatch(null), userActor);
                actorSystem.stop(userActor);
            });

            return NotUsed.getInstance();
        });
    }

    public boolean sameOriginCheck(Http.RequestHeader rh) {
        return true;
/*
        final String origin = rh.getHeader("Origin");

        if (origin == null) {
            logger.error("originCheck: rejecting request because no Origin header found");
            return false;
        } else if (originMatches(origin)) {
            logger.debug("originCheck: originValue = " + origin);
            return true;
        } else {
            logger.error("originCheck: rejecting request because Origin header value " + origin + " is not in the same origin");
            return false;
        }
*/
    }

    private boolean originMatches(String origin) {
        return origin.contains("localhost:9000") || origin.contains("localhost:19001");
    }


    public F.Either<Result, Flow<String, String, ?>> logException(Throwable throwable) {
        // https://docs.oracle.com/javase/tutorial/java/generics/capture.html
        Logger.error("Cannot create websocket", throwable);
        Result result = Results.internalServerError("error");
        return F.Either.Left(result);
    }

    private CompletionStage<F.Either<Result, Flow<String, String, ?>>> forbiddenResult() {
        final Result forbidden = Results.forbidden("forbidden");
        final F.Either<Result, Flow<String, String, ?>> left = F.Either.Left(forbidden);

        return CompletableFuture.completedFuture(left);
    }


    public CompletionStage<Result> getMetricsServicesVersion() {
        String url = getUrl(getVersionServicesUrls().getVersion());
        return ws.url(url).get().thenApply(rs -> rs.getBody()).thenApply(content -> ok(Json.parse(content)));
    }

    public Result versionInfo() {
        return ok(frontend.BuildInfo.toJson());
    }
}
