package actors;

import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import play.Logger;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by lrodriguez2002cu on 21/03/2017.
 */
public class LogRouter extends UntypedActor {

    Map<String, ActorRef> keyAndSocket = new HashMap<>();

    public static class RegisterMsg implements Serializable {
        ActorRef socketActorRef;
        String key;

        public RegisterMsg(ActorRef socketActorRef, String key) {
            this.socketActorRef = socketActorRef;
            this.key = key;
        }
    }

    public static class UnregisterMsg implements Serializable {
        String key;

        public UnregisterMsg(String key) {
            this.key = key;
        }
    }


    @Override
    public void onReceive(Object message) throws Throwable {
        if (message instanceof RegisterMsg) {
            keyAndSocket.put(((RegisterMsg) message).key, ((RegisterMsg) message).socketActorRef);
        } else if (message instanceof UnregisterMsg) {
            keyAndSocket.remove(((UnregisterMsg) message).key);
        } else if (message instanceof LogMessage) {
            if (keyAndSocket.containsKey(((LogMessage) message).consistentHashKey())) {
                ActorRef socketActorRef = keyAndSocket.get(((LogMessage) message).consistentHashKey());
                String content = ((LogMessage) message).getValue();
                socketActorRef.tell(content, self());

            } else Logger.warn("message to unregistered actor ref with  key: {}",
                    ((LogMessage) message).consistentHashKey());
        }
    }
}
