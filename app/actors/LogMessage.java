package actors;

import akka.routing.ConsistentHashingRouter;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.Serializable;

/**
 * Created by lrodriguez2002cu on 21/03/2017.
 */
public class LogMessage implements ConsistentHashingRouter.ConsistentHashable, Serializable{

    String value;
    String key;

    public LogMessage(String key, String value) {
        this.value = value;
        this.key = key;
    }

    @Override
    public Object consistentHashKey() {
        return key;
    }

    public String getValue(){
        return value;
    }
}
