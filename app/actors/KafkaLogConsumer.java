package actors;

import akka.NotUsed;
import akka.actor.*;
import akka.japi.function.Function;
import akka.kafka.ConsumerSettings;
import akka.kafka.KafkaConsumerActor;
import akka.kafka.Subscriptions;
import akka.kafka.javadsl.Consumer;
import akka.stream.ActorMaterializer;
import akka.stream.ActorMaterializerSettings;
import akka.stream.Materializer;
import akka.stream.Supervision;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;
import play.Logger;


/**
 * Created by lrodriguez2002cu on 19/03/2017.
 */
public class KafkaLogConsumer {

    public static final String METRICS_EXEC_LOG = "metrics_exec_log";

    //static Logger logger = LoggerFactory.getLogger(KafkaLogConsumer.class);

    public KafkaLogConsumer() {

    }

    static class LoggerActor extends UntypedActor {
        //Logger logger = LoggerFactory.getLogger(KafkaLogConsumer.class);

        public static Props props(){
            return Props.create(LoggerActor.class);
        }

        @Override
        public void onReceive(Object message) throws Throwable {
             Logger.info("Logged: {}", message);
        }
    }


    public static KafkaLogConsumer withLoggerSink(ActorSystem system, Config config) {
        KafkaLogConsumer consumer = new KafkaLogConsumer();
        ActorRef ref = system.actorOf(LoggerActor.props());
        final Sink<LogMessage, NotUsed> sink = Sink.actorRef(ref, new Status.Success("success"));
        consumer.start(system, config, sink);
        return consumer;
    }




    //region How to use rest kafka services
/*   # Produce a message using JSON with the value '{ "foo": "bar" }' to the topic test
    $ curl -X POST -H "Content-Type: application/vnd.kafka.json.v1+json" \
            --data '{"records":[{"value":{"foo":"bar"}}]}' "http://localhost:8082/topics/jsontest"
    {"offsets":[{"partition":0,"offset":0,"error_code":null,"error":null}],"key_schema_id":null,"value_schema_id":null}

    # Create a consumer for JSON data, starting at the beginning of the topic's
    # log. Then consume some data from a topic using the base URL in the first response.

    # Finally, close the consumer with a DELETE to make it leave the group and clean up
    # its resources.
    $ curl -X POST -H "Content-Type: application/vnd.kafka.v1+json" \
            --data '{"name": "my_consumer_instance", "format": "json", "auto.offset.reset": "smallest"}' \
    http://localhost:8082/consumers/my_json_consumer
    {"instance_id":"my_consumer_instance",
            "base_uri":"http://localhost:8082/consumers/my_json_consumer/instances/my_consumer_instance"}
    $ curl -X GET -H "Accept: application/vnd.kafka.json.v1+json" \
    http://localhost:8082/consumers/my_json_consumer/instances/my_consumer_instance/topics/jsontest
            [{"key":null,"value":{"foo":"bar"},"partition":0,"offset":0}]
    $ curl -X DELETE \
    http://localhost:8082/consumers/my_json_consumer/instances/my_consumer_instance

    */
    //endregion

    public void start(ActorSystem system, String configResourceBaseName, Sink<LogMessage, NotUsed> sink) {
        final ConsumerSettings<String, String> consumerSettings = getConsumerSettings(system, configResourceBaseName);
        start(system, sink, consumerSettings);
    }

    public void start(ActorSystem system, Config config, Sink<LogMessage, NotUsed> sink) {
        final ConsumerSettings<String, String> consumerSettings = getConsumerSettings(system, config);
        start(system, sink, consumerSettings);
    }

    private void start(ActorSystem system, Sink<LogMessage, NotUsed> sink, ConsumerSettings<String, String> consumerSettings) {

        final Function<Throwable, Supervision.Directive> decider = exc -> {
            //if (exc instanceof ArithmeticException)
            return Supervision.resume();
                /*else
                    return Supervision.stop();*/
        };

        //Function<Throwable, Supervision.Directive> decider1 = decider;
        final Materializer materializer = ActorMaterializer.create(ActorMaterializerSettings.create(system).withSupervisionStrategy(decider), system);

        //Consumer is represented by actor
        ActorRef consumer = system.actorOf((KafkaConsumerActor.props(consumerSettings)));

        //Manually assign topic partition to it
        Logger.info("Creating consumer ...");

        //Source<String, Consumer.Control> mappedSource = getSource(consumer);
        Logger.info("Creating the sink ...");
        final Sink<LogMessage, NotUsed> writeVariableSink = sink/*cassandraSinkActor(system)*/;

        Consumer.plainExternalSource(consumer, Subscriptions.assignment(new TopicPartition(
                        METRICS_EXEC_LOG, 0)/**/))
                .map(t -> {
                    play.Logger.info("Mapping ...");
                    String key = unquote((String) t.key());

                    String content = (String)t.value();
                    play.Logger.info("key: {}, value: {}", key, content);

                            /*KeyDetails keyDetails = KafkaLogConsumer.decodeKey(key);
                            String application = keyDetails.application;
                            String variable = keyDetails.variable;*/

                            /*logger.debug("getting variable from {} record key:{} value:{}", t.topic(), key, t.value());
                            logger.debug("Variables app:{} variable:{}", application, variable);
                            */
                           /* WriteInstruction writeInstruction = new WriteInstruction(variable, t.value().toString(), getWritingContext(getData(t), variable, "")
                                *//*new WriteInstruction.WriteContext("fail","fail","fail","fail","fail" )*//*
                            );*/

                    //logger.debug("Instruction: {}", writeInstruction);

                    /*ObjectMapper mapper = new ObjectMapper();
                    JsonNode jsonNode = mapper.readTree(content);*/

                    return  new LogMessage(key, content)/*jsonNode*/;//"message for:" + key;

                }).runWith(writeVariableSink, materializer);

        //mappedSource.runWith(writeVariableSink, materializer);
    }

    private String unquote(String key) {
        if (key.startsWith("\"") && key.endsWith("\"")) {
            return key.substring(1, key.length()-1);
        }
        return key;
    }

    public Source<String, Consumer.Control> getSource(ActorRef consumer) {
        return Consumer
                .plainExternalSource(consumer, Subscriptions.assignment(new TopicPartition(
                        METRICS_EXEC_LOG, 0)/**/))
                .map(t -> {
                    play.Logger.info("Mapping ...");
                    String key = (String) t.key();
                            /*KeyDetails keyDetails = KafkaLogConsumer.decodeKey(key);
                            String application = keyDetails.application;
                            String variable = keyDetails.variable;*/

                            /*logger.debug("getting variable from {} record key:{} value:{}", t.topic(), key, t.value());
                            logger.debug("Variables app:{} variable:{}", application, variable);
                            */
                           /* WriteInstruction writeInstruction = new WriteInstruction(variable, t.value().toString(), getWritingContext(getData(t), variable, "")
                                *//*new WriteInstruction.WriteContext("fail","fail","fail","fail","fail" )*//*
                            );*/

                    //logger.debug("Instruction: {}", writeInstruction);
                    return "message for:" + key;
                });
    }

    private ConsumerSettings<String, String> getConsumerSettings(ActorSystem system, String configResourceBaseName) {

        Config fullConfig = ConfigFactory.load(configResourceBaseName);

        Config config = fullConfig.getConfig("akka.kafka.consumer");

        return getConsumerSettings(system, config);
    }

    private ConsumerSettings<String, String> getConsumerSettings(ActorSystem system, Config config) {
        String bootstrapServers = config.getString("bootstrap.servers");
        Logger.info("Bootstrap servers: {}", bootstrapServers);
        if (config != null/*configResourceBaseName != null && !configResourceBaseName.isEmpty()*/) {
            Logger.info("Creating consumer settings from: {} ", "non default config.");
            return ConsumerSettings.create(config/*system*/, new StringDeserializer(), new StringDeserializer())
                    .withBootstrapServers(bootstrapServers)
                    //.withGroupId("group1")
                    .withProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        } else {
            Logger.info("Creating consumer settings from ActorSystem");
            return ConsumerSettings.create(system, new StringDeserializer(), new StringDeserializer())
                    .withBootstrapServers(bootstrapServers)
                    //.withGroupId("group1")
                    .withProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        }
    }




       /* public Sink<String, CompletionStage<IOResult>> lineSink(String filename) {
            return Flow.of(String.class)
                    .map(s -> ByteString.fromString(s.toString() + "\n"))
                    .toMat(FileIO.toPath(Paths.get(filename)), Keep.right());
        }*/


}
