package services;


/**
 * Created by lrodr_000 on 17/01/2017.
 */
public class UrlUtils {


    public static final String BASE_SERVICES_URL_KEY = "metrics-services-url";
    public static final String DOCS_URL_KEY = "metrics-docs-url";
    public static final String API_V1 = "/api/v1/";

    private static UrlUtils.ApplicationServicesUrls applicationServicesUrls;
    private static MetricsServicesUrls metricsServicesUrls;
    private static RunServicesUrls runServicesUrls;
    private static ExecutionParamsServicesUrls executionParamsServicesUrls;
    private static DataServicesUrls dataServicesUrls;

    private static VersionServiceUrls versionServiceUrls;


    public static VersionServiceUrls getVersionServicesUrls() {
        if (versionServiceUrls == null) {
            versionServiceUrls = new UrlUtils.VersionServiceUrls();
        }
        return versionServiceUrls;
    }
   public static RunServicesUrls getRunServicesUrls() {
        if (runServicesUrls == null) {
            runServicesUrls = new UrlUtils.RunServicesUrls();
        }
        return runServicesUrls;
    }

    public static ApplicationServicesUrls getAppServicesUrls() {
        if (applicationServicesUrls == null) {
            applicationServicesUrls = new UrlUtils.ApplicationServicesUrls();
        }
        return applicationServicesUrls;
    }


    public static DataServicesUrls getDataServicesUrls() {
        if (dataServicesUrls == null) {
            dataServicesUrls = new UrlUtils.DataServicesUrls();
        }
        return dataServicesUrls;
    }


    public static MetricsServicesUrls getMetricsServicesUrls() {
        if (metricsServicesUrls == null) {
            metricsServicesUrls = new UrlUtils.MetricsServicesUrls();
        }
        return metricsServicesUrls;
    }

    public static ExecutionParamsServicesUrls getExecutionParamsServicesUrls() {
        if (executionParamsServicesUrls == null) {
            executionParamsServicesUrls = new UrlUtils.ExecutionParamsServicesUrls();
        }
        return executionParamsServicesUrls;
    }

    public static abstract class AbstractBaseServicesUrls {

        public abstract String getBaseCreate();

        public abstract String getBaseUpdate();

        public abstract String getBaseExist();

        public abstract String getBaseSelect();

        public abstract Object getBaseDelete();


        public String getBaseList() {
            String baseSelect = getBaseSelect();
            if (baseSelect.endsWith("/")) {
                baseSelect = baseSelect.substring(0, baseSelect.length() - 1);
            }
            return baseSelect;
        }

    }

    public static abstract class BaseServicesUrls extends AbstractBaseServicesUrls {

    }

    public static class VersionServiceUrls {
        public static final String VERSION = API_V1 + "version";

        public String getVersion(){
            return  VERSION;
        }
    }

    public static class DataServicesUrls {

        public static final String DATASOURCES = API_V1 + "datasources/";
        public static final String DATA = API_V1 + "data/";
        public static final String METRICS_DATA = API_V1 + "metrics-data/";
        public static final String VARIABLE_DATA = API_V1 + "variable-data/";

        /*
                GET         /api/v1/datasources/:appid                       controllers.DataServices.getDataSources(appid: String)
                GET         /api/v1/data/:appid/:datasource                  controllers.DataServices.getDataForDatasource(appid: String, datasource: String)
                GET         /api/v1/metrics-data/:appid/:metric              controllers.DataServices.getDataForMetric(appid: String, metric: String)
                GET         /api/v1/variable-data/:appid/:variable           controllers.DataServices.getDataForVariable(appid: String, variable: String)
        */
        public String getDatasources(String appid) {
            return DATASOURCES + appid;
        }


        public String getData(String appid, String datasource) {
            return DATA + appid + "/" + datasource;
        }

        public String getMetricsData(String appid, String datasource) {
            return METRICS_DATA + appid + "/" + datasource;
        }

        public String getVariableData(String appid, String datasource) {
            return VARIABLE_DATA + appid + "/" + datasource;
        }

    }

    public static class ApplicationServicesUrls extends BaseServicesUrls {


        public static final String BASE_CREATE = API_V1 + "create-application/";
        public static final String BASE_UPDATE = API_V1 + "update-application/";
        public static final String BASE_DELETE = API_V1 + "delete-application/";
        public static final String BASE_SELECT = API_V1 + "applications/";
        public static final String BASE_EXIST = API_V1 + "application-exist/";
        public static final String EDITOR = API_V1 + "applications-editor-template";

        public String getBaseDelete() {
            return BASE_DELETE;
        }

        @Override
        public String getBaseCreate() {
            return BASE_CREATE;
        }

        @Override
        public String getBaseUpdate() {
            return BASE_UPDATE;
        }

        @Override
        public String getBaseExist() {
            return BASE_EXIST;
        }

        @Override
        public String getBaseSelect() {
            return BASE_SELECT;
        }

        public String getEditor() {
            return EDITOR;
        }

    }

    public static class MetricsServicesUrls extends BaseServicesUrls {

        public static final String BASE_CREATE = API_V1 + "create-metrics/";
        public static final String BASE_UPDATE = API_V1 + "update-metrics/";
        public static final String BASE_DELETE = API_V1 + "delete-metrics/";
        public static final String BASE_SELECT = API_V1 + "metrics/";
        public static final String BASE_EXIST = API_V1 + "metrics-exist/";
        public static final String INVALIDATE = API_V1 + "invalidate-metrics-cache";

        /*POST        /api/v1/invalidate-metrics-cache            controllers.MetricsServices.notifyCacheActorsInvalidateAll()
        POST        /api/v1/invalidate-metrics-cache/:appid     controllers.MetricsServices.notifyCacheActorsInvalidateApp(appid: String)
*/


        public String getBaseDelete() {
            return BASE_DELETE;
        }

        @Override
        public String getBaseCreate() {
            return BASE_CREATE;
        }

        @Override
        public String getBaseUpdate() {
            return BASE_UPDATE;
        }

        @Override
        public String getBaseExist() {
            return BASE_EXIST;
        }

        @Override
        public String getBaseSelect() {
            return BASE_SELECT;
        }

        public String getInvalidate() {
            return INVALIDATE;
        }

        public String getInvalidate(String appid) {
            return INVALIDATE + "/" + appid;
        }

    }

    public static class ExecutionParamsServicesUrls extends BaseServicesUrls {

        public static final String BASE_CREATE = API_V1 + "create-exec-params/";
        public static final String BASE_UPDATE = API_V1 + "update-exec-params/";
        public static final String BASE_DELETE = API_V1 + "delete-exec-params/";
        public static final String BASE_SELECT = API_V1 + "exec-params/";
        public static final String BASE_EXIST = API_V1 + "exec-params-exist/";
        public static final String EDITOR = API_V1 + "exec-params-editor-template";

        public String getBaseDelete() {
            return BASE_DELETE;
        }

        @Override
        public String getBaseCreate() {
            return BASE_CREATE;
        }

        @Override
        public String getBaseUpdate() {
            return BASE_UPDATE;
        }

        @Override
        public String getBaseExist() {
            return BASE_EXIST;
        }

        @Override
        public String getBaseSelect() {
            return BASE_SELECT;
        }

        public String getEditor() {
            return EDITOR;
        }
    }

    public static class RunServicesUrls extends BaseServicesUrls {

        public static final String RUN = API_V1 + "run/";
        /*public static final String BASE_UPDATE = API_V1 + "update-exec-params/";
        public static final String BASE_DELETE = API_V1 + "delete-exec-params/";
        public static final String BASE_SELECT = API_V1 + "exec-params/";
        public static final String BASE_EXIST = API_V1 + "exec-params-exist/";*/
        public static final String EDITOR = API_V1 + "run-params-editor-template/";

        public String getBaseDelete() {
            // return BASE_DELETE;
            throw new RuntimeException("Service  not supported");
        }

        @Override
        public String getBaseCreate() {
            return RUN;
            //throw new RuntimeException("Service  not supported");
        }

        public String getRun(String appid) {
            return RUN + appid;
        }

        @Override
        public String getBaseUpdate() {
            //return BASE_UPDATE;
            throw new RuntimeException("Service  not supported");

        }

        @Override
        public String getBaseExist() {
            //return BASE_EXIST;
            throw new RuntimeException("Service  not supported");

        }

        @Override
        public String getBaseSelect() {
            //return BASE_SELECT;
            throw new RuntimeException("Service  not supported");
        }

        public String getEditor() {
            return EDITOR;
        }
    }


}
