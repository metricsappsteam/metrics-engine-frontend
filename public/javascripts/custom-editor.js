/*@*
 * This template takes a single argument, a String containing a
 * message to display.
 *@
 @(templateUrl: String, createServiceUrl: String, updateServiceUrl: String, getServiceUrl: String, id: String, holder: String, mode: String, disableFields: String)

 @*
 * Get an `Html` object by calling the built-in Play welcome
 * template and passing a `String` message.
 * @play20.welcome(message, style = "Java")
 *@
 @*<div class="container" >
 <div class="row">
 <div id="editor_holder"></div>
 </div>
 <div class="row">
 <button id='submit' class="btn btn-default col-md-3">Update/Create</button>
 </div>

 <div class="row">
 <div id ="alert_placeholder"></div>
 </div>
 </div>*@

 <script language="JavaScript">*/

var editors = {};

jQuery["postJSON"] = function (url, data, callback) {
    // shift arguments if data argument was omitted
    if (jQuery.isFunction(data)) {
        callback = data;
        data = undefined;
    }

    return jQuery.ajax({
        url: url,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: data,
        success: callback
    });
};

function newEditorConfig(templateUrl, createServiceUrl, updateServiceUrl, getServiceUrl, id, holder, mode, disableFields, hiddenFields, editorConfig, title, messageOnNoForm) {

    var baseConfig = {
        disable_collapse: true,
        disable_properties: true,
        disable_edit_json: true,
        disable_array_delete_all_rows: true,
        disable_array_delete_last_row: true
    };

    Object.assign(baseConfig, editorConfig);

    return {
        "templateUrl": templateUrl,
        "createServiceUrl": createServiceUrl,
        "updateServiceUrl": updateServiceUrl,
        "getServiceUrl": getServiceUrl,
        "objid": id,
        "holder": holder,
        "mode": mode,
        "disableFields": disableFields,
        "editorConfig" : baseConfig,
        "createIfErrorLoading": false,
        "doCreateOnUpdate": false,
        "alert_placeholder_id": "alert_placeholder",
        "hiddenFields": hiddenFields,
        "title": title,
        "load_property_to_edit": null,
        "messageOnNoForm": messageOnNoForm
    };


}

//var elementById = document.getElementById(holder);
/*    var html = '<div class="container" > '
 + '  <div class="row"> '
 + '   <div id="editor_holder' + holder + '"' + '></div> '
 + '    </div> '
 + '    <div class="row"> '
 + '    <button id="submit" class="btn btn-default col-md-3">Update/Create</button> '
 + '    </div> '
 + '    <div class="row"> '
 + '    <div id ="alert_placeholder"></div> '
 + '    </div> '
 + '    </div> ';

 $("#"+holder).html(html);*/

//templateUrl, createServiceUrl, updateServiceUrl, getServiceUrl, id, holder, mode, disableFields
function DynamicEditor(config, onInit) {

    this.holder = config.holder;
    this.createServiceUrl = config.createServiceUrl;
    this.updateServiceUrl = config.updateServiceUrl;
    this.getServiceUrl = config.getServiceUrl;
    this.templateUrl = config.templateUrl;
    this.disableFields = config.disableFields;
    this.editorConfig = config.editorConfig;
    this.objid = config.objid;
    this.createIfErrorLoading = config.createIfErrorLoading;
    this.doCreateOnUpdate = config.doCreateOnUpdate;
    this.alert_placeholder_id = config.alert_placeholder_id;
    this.onInit = onInit;
    this.hiddenFields = config.hiddenFields;
    this.title = config.title;
    this.load_property_to_edit = config.load_property_to_edit;

    this.inner_editor = null;
    this.messageOnNoForm = (typeof config.messageOnNoForm != 'undefined'
                    && config.messageOnNoForm != null)? config.messageOnNoForm : "Nothing to display here.";


    var holder = this.holder;
    editors[holder] = this;

    this.init = function (templateUrl, holder, disableFields) {
        var ajv = new Ajv();

        JSONEditor.defaults.options.theme = 'bootstrap3';
        JSONEditor.defaults.options.iconlib = "bootstrap3";
        JSONEditor.plugins.epiceditor.basePath = 'epiceditor';
        JSONEditor.defaults.options.template = 'handlebars';

        // Custom validators must return an array of errors or an empty array if valid
        JSONEditor.defaults.custom_validators.push(function (schema, value, path) {
            var errors = [];
            if (schema.format === "json") {

                if (path.includes("schema") && value != ''
                ) {

                    try {
                        var valueObj = JSON.parse(value);

                        var schemaValid = ajv.validateSchema(valueObj);
                        if (!schemaValid) {

                            for (var err in ajv.errors) {
                                var errObj = ajv.errors[err];
                                var errMsg = errObj.dataPath + ': ' + errObj.keyword + ' ' + errObj.message;

                                errors.push({
                                    path: path,
                                    property: 'schema',
                                    message: errMsg
                                });

                                break;
                            }

                        }
                    }
                    catch (err) {
                        errors.push({
                            path: path,
                            property: 'schema',
                            message: 'Schemas provided must be a valid json:' + err
                        });
                    }
                }

            }
            return errors;
        });

        $.getJSON(templateUrl,
            function (data) {
                console.log('received:' + data);
                var editor = editors[holder];
                var editorConfig = editor.editorConfig;
                if (!data.error) {
                    editorConfig.schema = data;
                    editor.inner_editor = new JSONEditor(document.getElementById(/*'editor_holder' +*/ holder/*"editor_holder"*/), editorConfig);

                    var fieldsToDisable = disableFields!=null? disableFields.split(','): null;
                    var fieldsToHide = editor.hiddenFields!=null? editor.hiddenFields.split(','): null;

                    setupDisabledFields(fieldsToDisable, editor.inner_editor);
                    setupHiddenFields(fieldsToHide, editor.inner_editor);
                    //editor = editor.getEditor("root.variables");
                    if (editor.objid != '') {
                        loadData(editor.objid, holder);
                    }
                    if (typeof editor.onInit != 'undefined' && editor.onInit!= null){
                        onInit(editor);
                    }
                }
                else {
                    $('#'+holder).append('<div class="alert alert-info" role="alert"><span>'+editor.messageOnNoForm+'</span></div>')
                }

            });

    }
    this.init(this.templateUrl, this.holder, this.disableFields);

    this.reload = function() {
        $("#"+this.holder).empty();
        this.init(this.templateUrl, this.holder, this.disableFields);
    }
}

function reload(editorId) {
    var editor = editors[editorId];
    editor.reload();
}

function loadData(id, editorId) {
    if (id != null) {
        var editor = editors[editorId];
        editor.objid = id;
        var url = editor.getServiceUrl + '/' + id;
        $.getJSON(url,
            function (data) {
                console.log('received data to edit:' + data);
                if (!data.error) {
                    if (editor.load_property_to_edit == null) {
                        editor.inner_editor.setValue(data.result);
                    }
                    else {
                        if (typeof editor.load_property_to_edit == 'string') {
                            editor.inner_editor.setValue(data.result[editor.load_property_to_edit]);
                        }
                        else {
                            if (typeof editor.load_property_to_edit == 'function'){
                                editor.inner_editor.setValue(editor.load_property_to_edit(data.result));
                            }
                        }
                    }
                }
                else {
                    if (editor.createIfErrorLoading) {
                       console.log("error loading so create will be performed on save: " + data.errorMessage);
                       editor.doCreateOnUpdate = true;
                    }
                    else {
                        console.log("error:" + data.errorMessage);
                    }
                }
            });
    }
    else editors[editorId].setValue(null);
}

function getValue(editorId) {
    var editor = editors[editorId];
    var value = JSON.stringify(editor.inner_editor.getValue());
    var errors = editor.inner_editor.validate();

    console.log("Saving value: " + value);
    var result = { "errors": errors, "valid": errors.length==0,  "value": value};
    return result;
}

function saveData(id, editorId, callback) {
    var editor = editors[editorId];
    var value = JSON.stringify(editor.inner_editor.getValue());
    console.log("Saving value: " + value);
    var url = (editor.doCreateOnUpdate?editor.createServiceUrl : editor.updateServiceUrl) + '/' + id;

    $.postJSON(url, value, function (data, status, xhr) {
        if (!data.error) {
            console.log("Success saving data");
            showAlert(editorId, editor.title + " saved successfully.");
        }
        else {
            showError(editorId, data.errorMessage);
            console.log("error:" + data.errorMessage);
        }
        if (typeof callback != 'undefined') {
            callback(data)
        }

    });
}

function showError(holder, message) {

    var outEditor = getEditor(holder);
    //$('#'+outEditor.alert_placeholder_id).html('');
    $('#'+outEditor.alert_placeholder_id).append('<div class="alert   alert-danger" role="alert"><a class="close" data-dismiss="alert" >×</a><strong>Error!</strong><span>'+message+'</span></div>')

}


function showAlert(holder, message) {
    var outEditor = getEditor(holder);
    //$('#'+outEditor.alert_placeholder_id).html('');
    $('#'+outEditor.alert_placeholder_id).append('<div class="alert  alert-success" role="alert"><a class="close" data-dismiss="alert" >×</a><strong>Success!</strong><span>'+message+'</span></div>')

}


function saveRemote(outEditor, callback) {
    saveData(outEditor.objid, outEditor.holder, callback);
}


function saveToFile(outEditor, callback) {

    var editor = outEditor.inner_editor;

    var value = JSON.stringify(editor.getValue());
    var blob = new Blob([value], {type: "application/json;charset=utf-8"});

    var filename = getFileName();
    saveAs(blob, filename);
    if (typeof callback != 'undefined') {
        callback(filename);
    }

}


function getEditor(holder){
    return editors[holder];
}

function getInnerEditor(holder){
    return editors[holder].inner_editor;
}

function setValue(holder, property, value){
    editors[holder].inner_editor.getEditor('root.' + property).setValue(value);
}


/*function clearEditors(holder){
    return editors[holder];
}*/

function createData(editorId, fn) {

    var editor = editors[editorId];
    var url = editor.createServiceUrl;
    var value = JSON.stringify(editor.inner_editor.getValue());

    console.log("Saving value: " + value);
    $.postJSON(url, value, function (data, status, xhr) {
        console.log('Created!');
        if (!data.error) {
            //editor.setValue(data.result);
            console.log("Success creating data");

        }
        else console.log("error:" + data.errorMessage);
        if (typeof fn != 'undefined') fn(data);
    });
}

function setupDisabledFields(fields, editor) {
    if (fields != null) {
        for (var i = 0; i < fields.length; i++) {
            var field = fields[i];
            if (field != "") {
                var editor2 = editor.getEditor('root.' + field);
                if (typeof  editor2 != 'undefined' && editor2!= null) {
                    editor2.disable();
                    //editor.getEditor('root.' + field).disable();
                }
            }
        }
    }
}

function setupHiddenFields(fields, editor) {
    if (fields != null) {
        for (var i = 0; i < fields.length; i++) {
            var field = fields[i];
            if (field != "") {
                var editor2 = editor.getEditor('root.' + field);

                if (typeof  editor2 != 'undefined' && editor2!= null) {
                    editor2.options.hidden = true;
                }
            }
        }
    }
}

function save(holder, savefn, callback){

    var bootstrap_alert = {};
    var outEditor = editors[holder];

    bootstrap_alert.warning = function(messages) {

        $('#'+outEditor.alert_placeholder_id).html('');

        for (var i =0; i< messages.length; i++ ) {
            var message = messages[i].message;
            var path = messages[i].path;
            var property = messages[i].property;
            $('#'+outEditor.alert_placeholder_id).append('<div class="alert   alert-danger" role="alert"><a class="close" data-dismiss="alert" >×</a><strong>'+path + property+'</strong><span>'+message+'</span></div>')
        }
    };

    var editor = outEditor.inner_editor;
    // Validate the editor's current value against the schema
    var errors = editor.validate();
    var value = editor.getValue();

    //console.log(holder);
    //console.log(value);

    if(errors.length) {
        // errors is an array of objects, each with a `path`, `property`, and `message` parameter
        // `property` is the schema keyword that triggered the validation error (e.g. "minLength")
        // `path` is a dot separated path into the JSON object (e.g. "root.path.to.field")
        console.log(errors);
        bootstrap_alert.warning(errors);

    }
    else {
       savefn(outEditor, callback);
    }

}

function getFileName(){
    if (typeof lastName == 'undefined' || lastName == null){
        return 'schema.json';
    }
    else return lastName;
}

function handleFileSelect(evt) {
    var files = evt.target.files; // FileList object

    // files is a FileList of File objects. List some properties.
    //var output = [];
    var f = files[0];
    lastName = f.name;
    console.log(lastName);
    var reader = new FileReader();

    // Closure to capture the file information.
    reader.onload = (function(theFile) {
        return function(e) {
            var obj = JSON.parse(e.target.result);
            var editor = editors[handleFileSelect.holder].inner_editor;
            editor.setValue(obj);
        };
    })(f);

    // Read in the image file as a data URL.
    reader.readAsText(f, 'utf-8');
}

function setFileHandler(editorHolder) {
    var h = handleFileSelect;
    h.holder = editorHolder;
    $(document).on('fileselect', ':file', h);
    /*if (document.getElementById('files')!= null) {
        //document.getElementById('files').addEventListener('change', h, false);

    }*/

    $(document).on('change', ':file', function() {
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);
    });
}

