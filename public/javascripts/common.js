/**
 * Created by lrodriguez2002cu on 26/03/2017.
 */

function initSocket(baseUrl, holder, id) {
    var url =  baseUrl+ '?id='+ id;
    var logsWS = new WebSocket(url);
    $("#" + holder).text("");
    $("#"+ holder).append("Sending job ...");
    logsWS.onmessage = function (event) {
        console.log("Receiving message:" + "'"+ event.data+ "'");
        $("#" + holder).append(event.data + '\n');
    };
}

jQuery["getJSON"] = function (url, callback, errorCallback) {
    // shift arguments if data argument was omitted
    return jQuery.ajax({
        url: url,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: callback,
        error: errorCallback
    });
};


function fixGeoJson(geojsonFeature){
    if (typeof geojsonFeature.features != "undefined" && typeof geojsonFeature.features == "object"
        && !Array.isArray(geojsonFeature.features)) {
        //lets fix it
        var obj = geojsonFeature.features;
        var arr = [];
        arr.push(obj);
        geojsonFeature.features = arr;
    }

    return geojsonFeature;
    return geojsonFeature;
}

function fixGeoJsonText(geojsonText){

    //eliminate spaces
    var str = geojsonText.trim();

    //possibly remove the first and last quotes..
    if (str.indexOf('"')==0) str = str.substring(1);
    if (str.lastIndexOf('"')==str.length-1) str = str.substring(0, str.length-1);

    var fullUnquoted = str;

    function isQuoted(str){

        var isQuoted =true;
        var found = false;

        while  (isQuoted && str.indexOf('"')!=-1) {
            var indexOf = str.indexOf('"');
            var indexOfSlash = str.indexOf('\\');
            isQuoted = isQuoted && (indexOf == indexOfSlash+1)
            str = str.substring(indexOf + 1);
            found = true;
        }

        return isQuoted && found;

    }
    //unquote

    if (isQuoted(str)){
        //fullUnquoted = fullUnquoted.replace(new RegExp('\\"', 'g'), '"');
        fullUnquoted = fullUnquoted.replace(/\\"/g, '"');
    }

    return fullUnquoted;
}

function getFeature(geojson){
    var geojsonFeature = JSON.parse(fixGeoJsonText(geojson));
    geojsonFeature = fixGeoJson(geojsonFeature);
    return geojsonFeature;
}

function eliminateBackdrop(id){
    $('#'+id).modal('hide');
    $('body').removeClass('modal-open');
    $('.modal-backdrop').remove();
}