name := """metrics-engine-frontend"""

version := "1.0.9"

lazy val root = (project in file(".")).enablePlugins(PlayJava).enablePlugins(DockerPlugin).enablePlugins(BuildInfoPlugin).
  settings(
    buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion),
    buildInfoPackage := "frontend"
  )

buildInfoOptions += BuildInfoOption.ToJson


scalaVersion := "2.11.8"

resolvers += Resolver.mavenLocal

libraryDependencies += javaJdbc
libraryDependencies += cache
libraryDependencies += javaWs

libraryDependencies += "es.kibu.geoapis" % "metrics-common" % "1.0"
libraryDependencies += "es.kibu.geoapis" % "metrics-core" % "1.0"
libraryDependencies += "es.kibu.geoapis" % "metrics-server-common" % "1.0"

libraryDependencies += "com.typesafe.akka" %% "akka-stream-kafka" % "0.13"

/*
<dependency>
  <groupId>com.typesafe.akka</groupId>
  <artifactId>akka-stream-kafka_2.11</artifactId>
  <version>0.13</version>
</dependency>
*/

//resolvers += "Local Maven" at "d:/installers/maven/.m2/repository"
maintainer in Docker := "Luis E. Rodriguez"

//dockerUpdateLatest := true

dockerRepository := Some("reg.geotecuji.org")